ics-ans-ldap
===================

Ansible playbook to install ldap managed by IAM.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause

